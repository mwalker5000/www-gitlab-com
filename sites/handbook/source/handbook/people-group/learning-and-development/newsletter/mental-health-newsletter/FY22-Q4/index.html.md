---
layout: handbook-page-toc
title: FY22-Q4 L&D Mental Health Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This is the 5th edition of the Mental Health newsletter from the GitLab L&D team! This will be the final edition of the newsletter in it's current format. In FY23, the L&D team will explore new ways to share mental health and team member wellbeing resources.

## GitLab Resource Feature

Did you attend #wellbeing-week in December, 2021? This was the second iteration from Mental Health Awareness week, originally hosted in December 2020. During the week, we featured:

1. 3 Live Spekaer Series with a total of 121 team members in attendance
1. 7 interview snippets with Sid and Wendy with a total of 63 views
1. a temporary #wellbeing-week Slack channel with 48 followers
1. 82 views of the [three task system](https://www.youtube.com/watch?v=H5Sg3Gw8E0Y) - watch below!

<iframe width="560" height="315" src="https://www.youtube.com/embed/H5Sg3Gw8E0Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We're featuring material from Wellbeing Week throughout January in GitLab Learn - you can catch all the replays in our [Skill of the Month: Wellbeing channel](https://gitlab.edcast.com/channel/skill-of-the-month-fy22).

## Mental Health in the New Year

Starting a new year might bring on any range of emotions for yourself and members of the GitLab team. Here are some important mental health resources to review and share with colleagues, friends, and family.

| Resource | Description |
| ----- | ---------- |
| [Active Minds: Mental Health Resources for remote work](https://www.activeminds.org/about-mental-health/be-there/coronavirus/at-work-resources/) | Active Minds is "...the premier organization impacting young adults and mental health... directly reach[ing] close to 600,000 students each year through campus awareness campaigns, events, advocacy, outreach, and more." This resouce includes excellent conversation starters for checking in with people and validating their difficult experience in a remote setting |
| [World Health Organization: Mental Health and Covid-19](https://www.who.int/teams/mental-health-and-substance-use/mental-health-and-covid-19) | WHO's mental health resources specific to living during the Covid-19 pandemic, including resources, stories, and research from around the world |
| [Workplace Mental Health Article](https://www.understood.org/articles/en/workplace-mental-health-5-ways-to-support-employee-wellness) | A short summary article that cites important reserach, outlines strategies, and explains benefits of supporting teams during challenging times |
| [Gallup Wellbeing Reports](https://www.gallup.com/topic/category_wellbeing.aspx) | Dive into the research from Gallup to undersatnd wellbeing trends and findings from communities around the globe |


## Yerbo Slack App Updates

In December 2021, ~35 team members particiapted in a trial of Yerbo, a Slack app that helps teams and individuals measure their burnout risk and take action to improve engagement at work.

Read about the results and testimonials about the trial [here](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health/-/issues/6#note_802078710).

The L&D team is working through the approval process to continue use of the free version of the app for team members. Stay tuned for an announcement and further documentation coming soon!


## Discussion 

Do you have ideas for a new format for sharing wellbeing resources at GitLab? Let us know in the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W/thread/G018JT50VH7-1641835035.006100)!
