---
layout: handbook-page-toc
title: "Persona snippets"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## SDR Persona Snippets

### User personas
1. [Sasha](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/sasha) the Software Developer
1. [Devon](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/devon) the DevOps Engineer
1. [Delaney](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/delaney) the Development Team Lead
1. [Cameron](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/cameron) the Compliance Manager
1. [Parker](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/parker) the Product Manager
1. [Rachel](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/rachel) the Release Manager
1. [Sidney](/handbook/marketing/strategic-marketing/persona-snippets/user-personas/sidney) the Systems Administrator

### Buyer personas
1. [Alex](/handbook/marketing/strategic-marketing/persona-snippets/buyer-personas/alex) the Application Development Manager
1. [Dakota](/handbook/marketing/strategic-marketing/persona-snippets/buyer-personas/dakota) the Application Development Director
1. [Erin](/handbook/marketing/strategic-marketing/persona-snippets/buyer-personas/erin) the Application Development Executive (VP, etc.)
1. [Kennedy](/handbook/marketing/strategic-marketing/persona-snippets/buyer-personas/kennedy) the Infrastructure Engineering Director
1. [Casey](/handbook/marketing/strategic-marketing/persona-snippets/buyer-personas/casey) the Release and Change Management Director
