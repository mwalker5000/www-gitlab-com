---
layout: handbook-page-toc
title: "TAM Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Upcoming Webinars

We’d like to invite you to our free upcoming webinars during the month of May.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!


### May 2022

### Advanced CI/CD 
#### May 24th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_aWj_kz5XSY6rJg5gU82Sww)

### Introducción a GitLab
#### May 24th, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

¿Estas comenzando con GitLab? Te invitamos a este webinar, donde revisaremos qué es GitLab, cómo lo beneficia y el flujo de trabajo recomendado para permitirle aprovechar al máximo la plataforma.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_cp7RI389RpScOixaSe1wyA)

### Introducción a CI/CD con GitLab
#### May 26th, 2022 at 3:00PM-4:00PM Eastern Time/7:00-8:00 PM UTC

Ven y conoce qué es CI/CD y cómo puede beneficiar a tu equipo/organización. Cubriremos una descripción general de CI/CD y cómo se ve en GitLab. También cubriremos cómo comenzar con su primer pipeline de CI/CD en GitLab y los conceptos básicos de los GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_ztNbL4O3RSmBQjZSyYTQJg)

### DevSecOps/Compliance 
#### May 31st, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_tZ7ZCQSxRVaH8fwU8I9PSg)

### GitLab Administration on SaaS
#### June 6th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Learn how to manage groups, projects, permissions, and more as you embark on your journey as an Owner in GitLab SaaS. In this session, you will learn what you can and cannot control and customize on the SaaS platform, and come out a SME in administration.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_3xY05EQVTGipHEBVAkqmMQ)

### Intro to GitLab
#### June 9th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_SpBFgR7WQ9KwqRBMUygLXQ)

### Intro to CI/CD
#### June 14th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8i5GYE8TSGKNmBxgCyvhJw)

### Advanced CI/CD
#### June 21st, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_NFRuq6lMR9aWXgqc4Q19pA)

### DevSecOps Compliance
#### June 23rd, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_hhtmjEXMSSWKOCl7O5XjjA)





